const siteUrl = process.env.NODE_ENV === `dev` ? `http://localhost:8000` : `https://marija-marinkovic-m.bitbucket.io`;

module.exports = {
  siteMetadata: {
    title: `Marija Marinkovic - Personal Website`,
    author: `Marija Marinkovic`,
    description: `Portfolio. Personal presentation of featured works and thoughts.`,
    facebookAppId: `574056742979691`,
    siteUrl,
    twitterHandle: `Kladabra`
  },
  pathPrefix: `/`,
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/posts`,
        name: `posts`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`,
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 700,
              showCaptions: true,
              linkImagesToOriginal: true
            },
          },
          {
            resolve: `gatsby-remark-prismjs`,
            options: {
              inlineCodeMarker: null
            }
          },
          `gatsby-remark-copy-linked-files`,
        ],
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: `UA-119278210-1`,
        head: false,
        anonymize: true,
        respectDNT: true
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-plugin-sitemap`
    }
  ],
}
