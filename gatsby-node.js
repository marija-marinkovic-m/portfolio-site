const _each = require("lodash/each")
const Promise = require("bluebird")
const _path = require("path")
// const select = require(`unist-util-select`)
// const fs = require(`fs-extra`)

exports.onCreateNode = ({ node, boundActionCreators, getNode }) => {
  const { createNodeField } = boundActionCreators;
  let slug = null;

  if (node.internal.type === `MarkdownRemark`) {
    const fileNode = getNode(node.parent);
    const parsedFilePath = _path.parse(fileNode.relativePath);

    if (parsedFilePath.dir === ``) {
      slug = parsedFilePath.name;
    } else {
      slug = parsedFilePath.dir;
    }

    createNodeField({ node, name: `path`, value: `/${slug}/` });
    createNodeField({ node, name: `slug`, value: slug });
  }
};

exports.createPages = ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators

  return new Promise((resolve, reject) => {
    const query = graphql(`{
      allMarkdownRemark(
        filter: { frontmatter: { draft: { ne: true } } }
        limit: 1000
      ) {
        edges {
          node {
            fields {
              path
              slug
            }
          }
        }
      }
    }`);
    resolve(
      query.then(result => {
        if (result.errors) {
          console.log(result.errors)
          reject(result.errors)
        }

        // Create pages.
        _each(result.data.allMarkdownRemark.edges, edge => {
          const { path, slug } = edge.node.fields;
          const singleTemplate = RegExp(/\/projects\/\w+/).test(path) ? 'project' : 'blog-post';

          createPage({
            path,
            component: _path.resolve(`./src/templates/single-${singleTemplate}.js`),
            context: {
              slug
            }
          });
        });
      })
    );
  });
};
