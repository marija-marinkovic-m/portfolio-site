const aosConfig = {
  once: true,
  offset: 50
};

export default aosConfig;