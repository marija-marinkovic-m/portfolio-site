import React from 'react';
import PropTypes from 'prop-types';
import cn from'classnames';

function Alert ({children, type = 'info', onClose, className, ...otherProps}) {
  const addedClasses = className ? className.split(' ') : [];
  return <div className={cn(['alert', type, ...addedClasses])} {...otherProps}>
    { onClose && <span className="closebtn" onClick={onClose}>&times;</span>  }
    { children }
  </div>
}

Alert.propTypes = {
  type: PropTypes.oneOf(['danger', 'success', 'warning', 'info']),
  onClose: PropTypes.func
}

export default Alert;