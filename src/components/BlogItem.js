import React from 'react';
import Link from 'gatsby-link';

import Tags from './Tags';

function BlogItem ({path, title, date, excerpt, tags, readMore = false, ...otherProps}) {
  if (!path) return null;
  return (
    <div data-aos="fade-up" {...otherProps}>
      <Link to={path}>
        <h3 style={{color: '#19a8cc', marginBottom: (tags ? '0.4em' : '1em')}}>
          {title}
          <span style={{color: '#bbb', fontSize: '0.8rem', verticalAlign: 'middle'}}>&nbsp;-&nbsp;<i className="fa fa-calendar"></i>&nbsp;{date}</span>
        </h3>
      </Link>
      {tags && <Tags style={{marginBottom: '0.7em'}} tags={tags.split(',')} />}
      <p style={{marginBottom: '3em'}}>
        {excerpt}{"\n"}
        {readMore && <Link to={path}>more</Link>}
      </p>
    </div>
  );
}

export default BlogItem;