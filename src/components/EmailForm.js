import React from 'react';
import Alert from './Alert';

const FORM_ACTION = 'https://script.google.com/macros/s/AKfycbwGrV0vyC4x6JzRMufoh_ZAX1rYHUuFUsn-j58clMphm1gZCeg/exec';
const emailReg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

const initializeState = () => ({
  formData: {
    name: '',
    email: '',
    message: '',
    formDataNameOrder: '["name","message","email"]',
    formDataSendEmail: 'marija.marinkovic.m@gmail.com',
    formGoogleSheetName: 'responses'
  },
  formValidity: {
    name: false,
    email: false,
    message: false
  },
  showValidity: false,
  loading: false,
  msgSent: false,
  msgError: null
});

class EmailForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = initializeState();
    this.honeypotRef = null;
  }

  handleChange = (event) => {
    const { target } = event;
    const { formData: stateFormData, formValidity: stateFormValidity } = this.state;
    const formData = Object.assign({}, stateFormData, {
      [target.name]: target.value
    });

    const formValidity = Object.assign({}, stateFormValidity, {
      [target.name]: this.validateField(target.name, target.value)
    });

    this.setState({formData, formValidity});
  }

  validateField(fieldName, fieldValue) {
    switch(fieldName) {
      case 'email':
        return emailReg.test(fieldValue);
      
      case 'name':
      case 'message':
        return fieldValue && fieldValue.length;
      default:
        return true;
    }
  }

  validateHuman(honeypot) {
    if (honeypot) {  //if hidden form filled up
      console.log("Robot Detected!");
      return true;
    } else {
      console.log("Welcome Human!");
    }
  }

  get isFormValid() {
    const { formValidity } = this.state;
    const keys = Object.keys(formValidity);
    for (let i = 0; i < keys.length; i++) {
      if (formValidity[keys[i]] === false) {
        return false;
      }
    }
    return true;
  }

  sendEmail(url, data) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open('POST', url);
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) {
          return;
        }
        console.log(xhr.status, xhr.statusText);
        console.log(xhr.responseText);
        const response = JSON.parse(xhr.responseText);
        if (response.result === 'error') {
          reject('An Error Occurred. Please, try later.');
          return;
        }
        resolve(response);
      }

      const encoded = Object.keys(data)
        .map(k => encodeURIComponent(k) + "=" + encodeURIComponent(data[k]))
        .join('&');

      xhr.send(encoded);
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    if (this.state.loading) return;
    if (!this.isFormValid) {
      this.setState({showValidity: true});
      return;
    }

    const { formData: data } = this.state;
    const honeypotRef = this.honeypotRef;

    // spam?
    if (honeypotRef && this.validateHuman(honeypotRef.value)) {
      return;
    }
    // send email
    this.setState({
      loading: true,
      showValidity: false,
      msgError: null,
      msgSent: false
    }, () => {
      this.sendEmail(FORM_ACTION, data)
        .then(() => this.setState({
          loading: false,
          msgSent: true
        }))
        .catch((msgError) => this.setState({
          loading: false,
          msgError
        }));
    });
  }

  formReset = () => {
    this.setState(initializeState())
  }

  render() {
    const { formData, loading, msgSent, msgError, showValidity, formValidity } = this.state;

    if (msgSent) {
      return (
        <Alert type="success" data-aos="fade-up" onClose={this.formReset}>
          <em>Thanks</em> for contacting me! I will get back to you soon!
        </Alert>
      );
    }

    return (
      <form
        id="gform"
        method="post"
        action={FORM_ACTION}
        onSubmit={this.handleSubmit}>
        <label className="sr-only">This field should be kept blank</label>
        <input
          id="honeypot"
          type="text"
          name="honeypot"
          value=""
          style={{display: 'none'}}
          ref={(el) => {this.honeypotRef = el}} />
        <div className="row uniform 50%" style={{marginBottom: '25px'}}>
          <div className="6u 12u$(xsmall)" data-aos="fade-up">
            <input
              type="text"
              name="name"
              id="name"
              placeholder="Name"
              value={formData.name}
              onChange={this.handleChange} />

            { showValidity && !formValidity.name && <span className="error" data-aos="fade-down">Please, enter your name.</span> }
          </div>
          <div className="6u 12u$(xsmall)" data-aos="fade-up">
            <input
              type="text"
              name="email"
              id="email"
              placeholder="Email"
              value={formData.email}
              onChange={this.handleChange} />

              { showValidity && !formValidity.email && <span className="error" data-aos="fade-down">Please, enter valid email.</span> }
          </div>
          <div className="12u" data-aos="fade-up" data-aos-delay="100">
            <textarea
              name="message"
              id="message"
              placeholder="Message"
              rows="4"
              value={formData.message}
              onChange={this.handleChange} />

            { showValidity && !formValidity.message && <span className="error" data-aos="fade-down">Message is required.</span> }
          </div>
        </div>
        { msgError && <Alert type="danger">{msgError}</Alert> }
        <ul className="actions" data-aos="fade-up" data-aos-delay="200">
          <li><input type="submit" value="Send Message" disabled={loading} /></li>
        </ul>
      </form>
    );
  }
}

export default EmailForm;