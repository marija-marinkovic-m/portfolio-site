import React from 'react';
import cn from 'classnames';

const social = [
    { icon: 'linkedin', label: 'Linkedin', href: 'https://www.linkedin.com/in/marija-marinkovic-45a5b6109/' },
    { icon: 'github', label: 'Github', href: 'https://github.com/marija-marinkovic-m' },
    { icon: 'rss', label: 'Sitemap', href: 'https://marija-marinkovic-m.bitbucket.io/sitemap.xml' },
    { icon: 'twitter', label: 'Twitter', href: 'https://twitter.com/Kladabra' },
    { icon: 'facebook', label: 'Facebook', href: 'https://www.facebook.com/marinkovic.marija.m' },
    { icon: 'envelope-o', label: 'Email', href: 'mailto:marija.marinkovic.m@gmail.com' }
];

class Footer extends React.Component {
    render() {
        return (
            <div id="footer">
                <div className="inner">
                    <ul className="icons">
                        {social.map(link => (
                            <li key={link.icon}>
                                <a
                                    href={link.href}
                                    className={cn(['icon', `fa-${link.icon}`])}
                                    ref="nofollow noopener noreferrer"
                                    target="_blank">
                                    <span className="label">{link.label}</span>
                                </a>
                            </li>
                        ))}
                    </ul>

                    <ul className="copyright">
                        <li>
                            Built with <a href="https://reactjs.org/" target="_blank" rel="nofollow noopener noreferrer">React</a> &amp; <a href="https://www.gatsbyjs.org/" target="_blank" rel="nofollow noopener noreferrer">Gatsby</a>
                        
                            {/* <br />
                            <a href="https://html5up.net" target="_blank" rel="nofollow noopener noreferrer">html5up</a> */}
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default Footer
