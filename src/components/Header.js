import React from 'react';
import Link from 'gatsby-link';

import Footer from './Footer';
import MobileMenu from './MobileMenu';
import avatar from '../assets/images/avatar-lighten.png';
// import avatar from '../assets/images/marija.jpeg';

const GoogleDrive = () => (<svg style={{
    display: 'inline-block', verticalAlign: 'text-bottom',
    width: '14px', height: '18px', fill: '#19a8cc'
}} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M339 314.9L175.4 32h161.2l163.6 282.9H339zm-137.5 23.6L120.9 480h310.5L512 338.5H201.5zM154.1 67.4L0 338.5 80.6 480 237 208.8 154.1 67.4z"/></svg>);

const activeLinkStyle = {
    color: '#FFF' // #01a8cd
};

class Header extends React.Component {
    componentDidMount() {
        // decide between particlesJS and pt

        // const particlesConfig = require('../assets/particles-config');
        // require('particles.js/particles');
        // particlesJS('pt', particlesConfig);

        const floatySpace = require('../assets/pt');
        if (window.innerWidth >= 980) {
            floatySpace();
        }
    }

    renderMenu = () => (
        <ul className="main-menu">
            <li><Link to="/about" activeStyle={activeLinkStyle}>About</Link></li>
            <li><Link to="/projects" activeStyle={activeLinkStyle}>Portfolio</Link></li>
            <li><Link to="/blog" activeStyle={activeLinkStyle}>Blog</Link></li>
            <li><Link to="/contact" activeStyle={activeLinkStyle}>Contact</Link></li>
            <li><a href="https://docs.google.com/document/d/1EUYnQN-OltwXBQQ-hH7d3tcBa6MCG1VMRoC0JV4gGf4/edit?usp=sharing" rel="nofollow noopener noreferrer" target="_blank">Resume&nbsp;&nbsp;<GoogleDrive /></a></li>
        </ul>
    )

    render() {
        const MainMenu = this.renderMenu;
        return (
            <div>
            <header id="header">
                <div id="pt" className="canvas"></div>
                <div className="inner">
                    <Link to="/">
                        <figure className="image avatar">
                            <img src={avatar} alt="" />
                        </figure>
                        <h1><strong>I am Marija</strong>, web citizen and artisan.</h1>
                    </Link>

                    <MainMenu />
                </div>
                <Footer />
            </header>

            <MobileMenu>
                <MainMenu />
                <Footer />
            </MobileMenu>
            </div>
        )
    }
}

export default Header
