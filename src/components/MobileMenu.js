import React from 'react';
import Link from 'gatsby-link';
import cn from 'classnames';

// import Accordion from './Accordion';

class MobileMenu extends React.Component {
  state = {
    menuIsOpen: false
  }

  toggleMenu = () => this.setState(prevState => ({menuIsOpen: !prevState.menuIsOpen}))
  closeMenu = () => this.state.menuIsOpen && this.setState({menuIsOpen: false})


  render() {
    const {  menuIsOpen } = this.state;
    return (
      <div>
        { menuIsOpen && (<div
          onClickCapture={this.closeMenu}
          data-aos="fade-in"
          data-aos-duration="4000"
          className="mobileMenu open">
          { this.props.children }
        </div>) }
        <button
          type="button"
          onClick={this.toggleMenu}
          className={cn(['hamburger', 'hamburger--spin', 'mobile'], {'is-active': menuIsOpen})}>
          <span className="hamburger-box">
            <span className="hamburger-inner" style={{zIndex: 9999}}></span>
          </span>
        </button>
      </div>
    );
  }
}

export default MobileMenu;