import React from 'react';
import PropTypes from 'prop-types';

const Modal = ({ children, close, title = '', closeBtn = true, ...other }) => (
  <div id="modal-root">
    <div
      className="outer"
      data-aos="fade-in"
      {...other}>
      <div
        className="overlay"
        onClick={close}></div>
      <div className="body">
        {/* close Button */}
        { closeBtn && <span className="closebtn" onClick={close}>&times;</span> }
        {/* modal title */}
        { title && <h3 style={{paddingRight: '1em'}}>{ title }</h3> }

        {/* content */}
        {children}
      </div>
    </div>
  </div>
);
Modal.propTypes = {
  close: PropTypes.func.isRequired,
  title: PropTypes.string,
  closeBtn: PropTypes.bool
};

export default Modal;