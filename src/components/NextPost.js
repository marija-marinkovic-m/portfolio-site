import React from 'react';
import PropTypes from 'prop-types';
import Link from 'gatsby-link';

function NextPost({ to, title, ...other }) {
  if (!to) return null;

  return (
    <div className="next-link" {...other}>
      <p>
        <Link to={to}>
          {`Next: ${title}`}
        </Link>
      </p>
    </div>
  );
}

NextPost.propTypes = {
  to: PropTypes.string,
  title: PropTypes.string
};

export default NextPost;