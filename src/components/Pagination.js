import React from 'react';
import PropTypes from 'prop-types';
import Link from 'gatsby-link';

function Pagination({previous, index, next}) {
  return (
    <div className="single-pagination">
      { previous && <Link to={previous}>{`◀`}</Link> }
      <Link className="back-to-index" to={index}>{`●`}</Link>
      { next && <Link to={next}>{`▶`}</Link> }
    </div>
  );
}

Pagination.propTypes = {
  previous: PropTypes.string,
  next: PropTypes.string,
  index: PropTypes.string
}
Pagination.defaultProps = {
  index: '/'
}
export default Pagination;