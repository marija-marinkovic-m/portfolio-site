import React from 'react';
import PropTypes from 'prop-types';
import Link from 'gatsby-link';
import cn from 'classnames';

// import defaultThumbnail from '../pages/seo-img-frontpage.png';

const dateStyle = {
  display: 'block',
  color: '#bbb', fontSize: '0.6rem'
}
const readMoreLinkStyle = {
  fontSize: '0.8rem'
}

function PostItem ({
  path,
  thumbnail,
  thumbAttr = 'Demo',
  title = 'Untitled',
  excerpt,
  date,
  inline = false,
  className,
  ...otherProps
}) {
  if (!path) return null;
  const addedClasses = className ? className.split(' ') : [];
  return (
    <article className={cn(['work-item', ...addedClasses], {inline})} {...otherProps}>
      {thumbnail && <Link
          className="image fit thumb"
          style={{backgroundImage: `url(${thumbnail})`}}
          data-content={thumbAttr}
          to={path}>
          <img src={thumbnail} alt={title} />
      </Link>}

      <div>
        <Link to={path}>
        <h3>
          {title}
          {date && <span style={dateStyle}><i className="fa fa-calendar"></i>&nbsp;{date}</span>}
        </h3>
        </Link>
        
        <p>{excerpt}</p>
        { inline && <Link to={path} style={readMoreLinkStyle}>Read</Link> }
      </div>
    </article>
  );
}
PostItem.propTypes = {
  path: PropTypes.string,
  thumbnail: PropTypes.string,
  title: PropTypes.string,
  excerpt: PropTypes.string,
  inline: PropTypes.bool,
  thumbAttr: PropTypes.string
}

export default PostItem;
