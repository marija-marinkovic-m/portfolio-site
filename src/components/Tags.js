import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

function Tags ({tags, className, ...otherProps}) {
  if (!tags) return null;
  const addedClasses = className ? className.split(' ') : [];
  return (
    <ul className={cn(['tags', ...addedClasses])} {...otherProps}>
      {tags.map((tag,i) => {
        return <li key={i}><span className="tag">{tag}</span></li>
      })}
    </ul>
  );
}

Tags.propTypes = {
  tags: PropTypes.arrayOf(PropTypes.string)
}
export default Tags;