import React from 'react'
import PropTypes from 'prop-types';
import Header from '../components/Header';

import 'prismjs/themes/prism.css';
import '../assets/scss/main.scss';

class Template extends React.Component {
    render() {
        const { children } = this.props

        return (
            <div>
                <Header />
                {children()}
            </div>
        )
    }
}

Template.propTypes = {
    children: PropTypes.func
}

export default Template
