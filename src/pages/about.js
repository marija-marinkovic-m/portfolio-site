import React from 'react';
import SeoMetaTags from '../components/SeoMetaTags';

import seoImage from './seo-img-frontpage.png';

export default class AboutPage extends React.Component {
  componentDidMount() {
    const aos = require('aos');
    const config = require('../assets/aos-config');
    aos.init(config);
  }
  render () {
    const meta = this.props.data.site.siteMetadata;
    return (<div>
      <SeoMetaTags
        author={meta.author}
        description={meta.description}
        facebookAppId={meta.facebookAppId}
        image={{ url: meta.siteUrl + seoImage, width: 1000, height: 500 }}
        logo={{ url: `About | ${meta.siteUrl}/logo-structured-data.png`, width: 600, height: 60 }}
        publisher={meta.title}
        title={meta.title}
        twitterHandle={meta.twitterHandle}
        type="website"
        url={meta.siteUrl} />
      <div id="main">
        <h1 data-aos="fade-up">About</h1>

        <blockquote data-aos="fade-up">
          To err is human; to edit, divine.
        </blockquote>
        
       <p data-aos="fade-up">I was born in a small <a href="https://goo.gl/maps/zqsbewekiE52" target="_blank">town</a>, in the middle of Balkans. It was the year 1986, and Metallica was preparing to release the <em>Master of Puppets</em>; the first virus in the PC history, <strong>Brain</strong> began its journey; the <strong>Chernobyl</strong> disaster started steam-blasting...</p>
        <p data-aos="fade-up">
        I was growing up in the country of <a href="https://www.iwp.edu/news_publications/detail/the-seven-states-of-the-former-yugoslavia-an-evaluation" target="_blank">Yugoslavia</a>, in the midst of its disintegration, on the rise of a civil war. But it was a good childhood, with lots of outdoor sports, play, socializing and folk music. There were no computers, no internet, even mobile phones were science fiction at a time. So, you can imagine my fascination with <em>Pentium II</em> that was a donation to our elementary schools' teaching equipment. I was so mesmerized by the most straightforward PowerPoint presentation! <br />My first personal computer was the same kind of <em>Deux ex Machina</em>, and I approached it with the most profound respect.</p>

        <blockquote data-aos="fade-up"><strong><em>"Hello, <em>Deux ex Machina</em>, we meet again,"</em></strong></blockquote>
        
        <p data-aos="fade-up">The year 2004, I was studying the <a href="http://www.ekfak.kg.ac.rs/en/" target="_blank">Faculty of Economics</a>, and working part-time in a casino. On the fourth semester, I dropped economics and switched to applied arts, philosophy, and bohemia, waiting tables for a living. Finally, eight years ago, I moved to the capital city and graduated from Graphic Design Department, <a href="https://www.politehnika.edu.rs" target="_blank">Polytechnics</a>, the University of Belgrade. While still on the lectures, I've started working for the <a href="https://eutelnet.com">Eutelnet agency</a>, and this turned out to be the most significant milestone so far.<br /> 
        <strong><em>"Hello, <em>Deux ex Machina</em>, we meet again,"</em></strong> but this time I was <strong>the mason</strong>, with tools to speak to the "beast" and build upon it.</p>
   
        <p>
        All of a sudden, I faced that same force that pulled me a decade before in my hometown' elementary school,... the same curiosity that drives creativity, the same appreciation as a fuel for making, sustains me to this very day.
        <br />
        It started with <em>Wordpress</em> and <em>jQuery</em> and simple presentations for lawyers or chiropractics. Now, we're developing SaaS solutions, innovating in the fields of education, management, and entrepreneurship, leveraging modern technologies like <em>Node.js</em> and <em>GraphQL</em>.</p>
        
        <p data-aos="fade-up">In the meantime, I became an eager traveler. Excited from witnessing this miracle every day: <em>humans devotedly enhancing technology and technology extending humans in return</em>. Almost perfect Perpetuum mobile! But it is rapidly changing the world as we know it, is's ironing all of the cultural, historical and even biological differences between nations and continents. There is a little time left to catch a glimpse of the passing centuries.</p>

        {/* <p>Halley's comet is the first naked-eye comet to be imaged up close by interplanetary spacecraft, which happened during its last visit, in 1986. Today I'm still patiently waiting for the Halley's Comet. It is next expected to visit the inner solar system in around 2061. This time it will not catch me by surprise, but it may be surprised itself by the Earths' unexpected turns :)</p> */}
      </div>
    </div>);
  }
}

export const aboutPageQuery = graphql`
  query AboutQuery {
    site {
      siteMetadata {
        title
        description
        author
        facebookAppId
        twitterHandle
        siteUrl
      }
    }
  }
`;