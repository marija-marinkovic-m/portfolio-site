import React from 'react';
import Link from 'gatsby-link';

import { SeoMetaTags, BlogItem } from '../components';
import seoImage from './seo-img-frontpage.png';

export default class BlogIndex extends React.Component {

  componentDidMount() {
    const aos = require('aos');
    const config = require('../assets/aos-config');
    aos.init(config);
  }

  render () {
    const { data } = this.props;
    const posts = data.allMarkdownRemark.edges;
    const meta = data.site.siteMetadata;
    return (<div>
      <SeoMetaTags
        datePublished={data.site.buildTime}
        author={meta.author}
        description={meta.description}
        facebookAppId={meta.facebookAppId}
        image={{url: seoImage, width: 1000, height: 500}}
        logo={{url: `${meta.siteUrl}/logo-structured-data.png`, width: 600, height: 60}}
        publisher={meta.title}
        title={`Blog | ${meta.title}`}
        twitterHandle={meta.twitterHandle}
        type="website"
        url={meta.siteUrl + '/blog'} />
      <div id="main">
        <h1 data-aos="fade-up">Blog</h1>
        {posts.map(({node}, i) => (
          <BlogItem
            key={node.id}
            data-aos-delay={String(i*100)}
            path={node.fields.path}
            title={node.frontmatter.title}
            date={node.frontmatter.date}
            tags={node.frontmatter.tags}
            excerpt={node.excerpt}
            readMore />
        ))}
      </div>
    </div>);
  }
}

export const blogQuery = graphql`
  query BlogQuery {
    site {
      buildTime
      siteMetadata {
        title
        author
        description
        facebookAppId
        twitterHandle
        siteUrl
      }
    }
    allMarkdownRemark(
      filter: {
        fields: {
          slug: {regex: "/blog\//"}
        },
        frontmatter: {
          draft: {ne: true}
        }
      }
      sort:{fields:[frontmatter___date], order:DESC}
    ) {
      totalCount
      edges{
        node{
          id
          frontmatter{
            title
            tags
            date(formatString:"DD MMMM YYYY")
          }
          fields {
            path
          }
          excerpt
        }
      }
      pageInfo{
        hasNextPage
      }
    }
  }
`;