import React from 'react';
import Link from 'gatsby-link';

import { SeoMetaTags, EmailForm } from '../components';
import seoImage from './seo-img-frontpage.png';

export default class ContactPage extends React.Component {

  componentDidMount() {
    const aos = require('aos');
    const config = require('../assets/aos-config');
    aos.init(config);
  }

  render () {
    const { data } = this.props;
    const meta = data.site.siteMetadata;
    return (<div>
      <SeoMetaTags
        datePublished={data.site.buildTime}
        author={meta.author}
        description={meta.description}
        facebookAppId={meta.facebookAppId}
        image={{url: seoImage, width: 1000, height: 500}}
        logo={{url: `${meta.siteUrl}/logo-structured-data.png`, width: 600, height: 60}}
        publisher={meta.title}
        title={`Contact | ${meta.title}`}
        twitterHandle={meta.twitterHandle}
        type="website"
        url={meta.siteUrl + '/contact'} />
      <div id="main">
        <h1 data-aos="fade-up">Get In Touch</h1>
        <p>If you have a project, you would like me to work on, or if you have anything we could collaborate on, please feel free to get in touch.</p>

        <EmailForm />
      </div>
    </div>);
  }
}

export const contactQuery = graphql`
  query ContactPageQuery {
    site {
      buildTime
      siteMetadata {
        title
        author
        description
        facebookAppId
        twitterHandle
        siteUrl
      }
    }
  }
`;