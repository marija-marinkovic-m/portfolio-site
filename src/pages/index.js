import React from 'react';
import Link from 'gatsby-link';
import get from 'lodash/get';

import { EmailForm, Accordion, PostItem, SeoMetaTags, BlogItem } from '../components';
import seoImage from './seo-img-frontpage.png';
import signature from '../assets/images/signature.png';


class HomeIndex extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            aboutExpandable: 0
        };
    }

    componentDidMount() {
        const aos = require('aos');
        const config = require('../assets/aos-config');
        aos.init(config);
    }

    toggleAboutExpandable = () => {
        const { aboutExpandable } = this.state;

        this.setState({
            aboutExpandable: aboutExpandable === 0 ? 'auto' : 0
        });
    }

    render() {
        const { aboutExpandable } = this.state;
        const meta = this.props.data.site.siteMetadata;
        const { edges: portfolioItems } = this.props.data.projects;
        const { edges: blogPosts } = this.props.data.blog;

        return (
            <div>
                <SeoMetaTags
                    author={meta.author}
                    description={meta.description}
                    facebookAppId={meta.facebookAppId}
                    image={{ url: meta.siteUrl + seoImage, width: 1000, height: 500 }}
                    logo={{ url: `${meta.siteUrl}/logo-structured-data.png`, width: 600, height: 60 }}
                    publisher={meta.title}
                    title={meta.title}
                    twitterHandle={meta.twitterHandle}
                    type="website"
                    url={meta.siteUrl} />

                <div id="main">

                    <section id="one">
                        <header className="major">
                            <h2 data-aos="fade-up">User Experience & User Interface&nbsp;
                            
                            <img src={signature} width="300" style={{opacity: 0.6, display: 'block'}} alt="Marija Marinkovic Signature" />
                            </h2>

                        
                        </header>
                        <p data-aos="fade-up">Hello, I am a software developer focused on crafting clean and efficient, useful and inspiring web user experiences. I love creating, buying, testing, evaluating and learning about new technology and have already spent a good part of my life doing that as a front-end developer speaking JavaScript.</p>

                        <Accordion duration={500} height={aboutExpandable}>
                            <p>Although I don't write as often as I would like, here I'm building a collection of some of my common observations, mostly work related: </p>
                            {blogPosts.map(({
                                node: {
                                    id,
                                    fields: {path},
                                    frontmatter: {title, date, tags},
                                    excerpt
                                }
                            }) => {
                                return <BlogItem
                                    key={id}
                                    path={path}
                                    title={title}
                                    excerpt={excerpt}
                                    date={date}
                                    tags={tags} />
                            })}
                        </Accordion>

                        <ul className="actions">
                            <li><button data-aos="fade-up" onClick={this.toggleAboutExpandable} className="button">Show {aboutExpandable === 0 ? 'More' : 'Less'}</button></li>
                        </ul>
                    </section>

                    <section id="two">
                        <h2 data-aos="fade-up">Featured Work</h2>

                        <div className="row" data-aos="fade-up">
                            { portfolioItems.map(({
                                node: {
                                    id,
                                    excerpt,
                                    fields: {
                                        path
                                    },
                                    frontmatter: {
                                        seoImage,
                                        title
                                    }
                                }
                            }) => {
                                const thumb = get(seoImage, 'childImageSharp.thumb.src');
                                return (<PostItem
                                    key={id}
                                    thumbnail={thumb}
                                    thumbAttr="More"
                                    title={title}
                                    excerpt={excerpt}
                                    className="4u 12u$(xsmall)"
                                    path={path} />);
                             }) }
                        </div>

                        <ul className="actions" data-aos="fade-up">
                            <li><Link to="/projects" className="button">Full Portfolio</Link></li>
                        </ul>
                    </section>

                    <section id="testimonials" data-aos="fade-up">
                        <blockquote>
                            <p>I've worked with Marija to produce exceptional projects for clients. Marija produces highly functional, clean code and can deliver even the most complex tasks in a timely manner.</p>
                            <p>One of the rare developers who are capable to produce quality work with such precision and dedication. Very demanding when it comes to details and skilled in multi-level environments, never refused challenge when she is confident she will succeed. Truly valuable member of our team.</p>
                            <cite>- <strong>Dejan Gligorijevic</strong>, Senior Project Manager at <a href="https://www.linkedin.com/company/whitecitysoft-belgrade-serbia/" target="_blank" rel="nofollow noopener noreferrer">White City Soft</a></cite>
                        </blockquote>
                    </section>

                    <section id="contact-section" data-aos="fade-up">
                        <h2 data-aos="fade-up">Get In Touch</h2>
                        <div className="row">
                            <div className="8u 12u$(small)">
                                <EmailForm />
                            </div>
                            <div className="4u 12u$(small)">
                                <ul className="labeled-icons">
                                    <li>
                                        <h3 className="icon fa-home"><span className="label">Address</span></h3>
                                        16th October st. 11<br />
                                        11000 Belgrade, Serbia
                                    </li>
                                    <li>
                                        <h3 className="icon fa-mobile"><span className="label">Phone</span></h3>
                                        (+381) 069 650-378
                                    </li>
                                    <li>
                                        <h3 className="icon fa-envelope-o"><span className="label">Email</span></h3>
                                        <a className="text-ellipsis" href="mailto:marija.marinkovic.m@gmail.com">marija.marinkovic.m@gmail.com</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </section>

                </div>

            </div>
        )
    }
}

export default HomeIndex

export const pageQuery = graphql`
    query PageQuery {
        site {
            siteMetadata {
                title
                description
                author
                facebookAppId
                twitterHandle
                siteUrl
            }
        }
        projects: allMarkdownRemark(
            filter:{
                frontmatter:{draft:{ne:true}}
                fields:{slug:{regex:"/projects\//"}}
            }
            sort:{fields:[frontmatter___title], order:ASC}
            limit:3
        ) {
            edges {
                node {
                    id
                    fields {
                        path
                    }
                    frontmatter {
                        title
                        seoImage {
                            childImageSharp {
                                thumb: resize(width: 300, quality: 90) {
                                    src
                                }
                            }
                        }
                    }
                    excerpt
                }
            }
        }
        blog: allMarkdownRemark(
            filter: {
                frontmatter:{draft:{ne:true}}
                fields:{slug:{regex:"/blog\//"}}
            }
            sort:{fields:[frontmatter___date],order:DESC}
            limit:3
        ) {
            edges {
                node {
                    id
                    fields{
                        path
                    }
                    frontmatter {
                        title
                        date(formatString:"DD MMMM YYYY")
                        tags
                    }
                    excerpt
                }
            }
        }
    }
`