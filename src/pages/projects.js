import React from 'react';
import Link from 'gatsby-link';
import get from 'lodash/get';

import { SeoMetaTags, PostItem } from '../components';
import seoImage from './seo-img-frontpage.png';

export default class ProjectsIndex extends React.Component {
  componentDidMount() {
    const aos = require('aos');
    const config = require('../assets/aos-config');
    aos.init(config);
  }

  render() {
    const { data } = this.props;
    const posts = data.allMarkdownRemark.edges;
    const meta = data.site.siteMetadata;
    return (<div>
      <SeoMetaTags
        datePublished={data.site.buildTime}
        author={meta.author}
        description={meta.description}
        facebookAppId={meta.facebookAppId}
        image={{url: seoImage, width: 1000, height: 500}}
        logo={{url: `${meta.siteUrl}/logo-structured-data.png`, width: 600, height: 60}}
        publisher={meta.title}
        title={`Portfolio | ${meta.title}`}
        twitterHandle={meta.twitterHandle}
        type="website"
        url={meta.siteUrl + '/projects'} />
      <div id="main">
        <h1 data-aos="fade-up">Featured Projects</h1>
        {posts.map(({node}, i) => {
          const thumbSrc = get(node, 'frontmatter.seoImage.childImageSharp.resize.src');
          return <PostItem
            key={node.id} 
            data-aos="fade-up"
            data-aos-delay={String(i * 100)}
            path={node.fields.path}
            thumbnail={thumbSrc}
            thumbAttr="Read more"
            title={node.frontmatter.title}
            excerpt={node.excerpt}
            date={node.frontmatter.date}
            inline />
        })}
      </div>
    </div>);
  }
}

export const projectsQuery = graphql`
  query ProjectsQuery {
    site {
      buildTime
      siteMetadata {
        title
        author
        description
        facebookAppId
        twitterHandle
        siteUrl
      }
    }
    allMarkdownRemark(
      filter: {
        fields: {
          slug: {regex: "/projects\//"}
        },
        frontmatter: {
          draft: {ne: true}
        }
      }
      sort:{fields:[frontmatter___date], order:DESC}
    ) {
      totalCount
      edges{
        node{
          id
          frontmatter{
            title
            date(formatString: "YYYY, MMMM")
            seoImage {
              childImageSharp {
                resize(width: 380, quality: 100) {
                  src
                }
              }
            }
          }
          fields {
            path
          }
          excerpt
        }
      }
      pageInfo{
        hasNextPage
      }
    }
  }
`;