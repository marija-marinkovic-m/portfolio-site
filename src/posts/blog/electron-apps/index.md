---
title: Electron Power Plant
draft: false
date: "2018-05-17"
seoImage: seo-img.png
tags: Electron,Next.js,ReactJS,NodeJS
---
Last week I was researching for some tangible way to empower an enterprise application platform we will be working on in the following months. This application which will manage all aspects of the organization's operations and communications should, among other possibilities, be "able" to read and write files on a users' computer, to use computer speakers and, eventually, the camera. Web pages in standard browsers have limited access to native resources. But our JavaScript knowledge can be leveraged with NodeJS, which extends the power of web pages allowing lower level operating system interactions (via C++).

![Electron Power Plant](./electron-power-plant.png)

[Electron](https://electronjs.org) library is the proven business solution across web community, companies like Slack, Atom, Microsoft, Wordpress.com are already using it, here is the ever-growing list of electron users: [https://electronjs.org/apps](https://electronjs.org/apps). A few years ago I was also experimenting with Electron on the [Team Scheduler](/projects/team-schedule) side project.

The Electron is an open source library developed by GitHub for building cross-platform desktop applications with HTML, CSS, and JavaScript. Electron accomplishes this by combining Chromium and Node.js into a single runtime and apps can be packaged for Mac, Windows, and Linux [(more)](http://electron.atom.io/docs/).

## Implementation

Desktop app configuration will impose another layer of complexity to our setup. Especially after we implemented [SSR](https://ssr.vuejs.org/en/). Since [Next.js](https://github.com/zeit/next.js/) was our tool of choice, all of the `BrowserWindow` instances should be pointing to the build.

The `main` process creates web pages by creating `BrowserWindow` instances. Each `BrowserWindow` instance runs the web page in its renderer process. When a `BrowserWindow` instance is destroyed, the corresponding renderer process is also terminated. The `main` process manages all web pages and their corresponding renderer processes. Each renderer process is isolated and only cares about the web page running in it.

![Check out this great article on Manning.com https://freecontent.manning.com/electron-in-action-article-1/](./manning.png "Check out this great article on Manning.com https://freecontent.manning.com/electron-in-action-article-1/")

Communication between processes can also be accomplished via various IPC modules (for example, ipcRenderer and ipcMain modules for sending messages, and the remote module for RPC style communication…). 

### A matter of some concern

The central notice for other people (outside of the front-end team) is specific CI requirements of this new front-end approach. How will we ship updates to the end users and how will this affect DevOps people?

As I’ve already mentioned, we have the bulk of IPC (inter-process communication) modules at the disposal. IPC allows subscribing to messages on a channel and sending messages to channel subscribers.

So we’ll be using specific IPC configuration for the built-in auto-updater channel. The auto-updater is (structurally) a part of the electron-builder package and (functionally) a part of application’s `main` process. It emits events like checking-for-updates, update-available, update-downloaded and can perform various methods like checkForUpdates, quitAndInstall, etc. [(detailed manual here)]()

On macOS, the auto-updater module is built upon a [Squirrel](https://github.com/Squirrel/Squirrel.Mac),  so, the application must be signed for automatic updates on macOS. Which is a requirement of the Squirrel.

For Windows users, the situation is a bit more complicated. The application has to be installed on a user’s machine before the auto-updater can be used, so it is recommended to generate a windows installer as part of the build process.