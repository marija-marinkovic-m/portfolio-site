---
title: Preprocessing is dead, long live preprocessing
date: "2018-02-03"
seoImage: seo-img.png
tags: css,CSS-in-JS,CSS Modules
---

> "Evolution is boring, democracy is boring, and standards (and even more the process of defining and improving them) are very boring..." -- Markus Oberlehner on [CSS-in-JS](https://medium.com/netscape/revolution-vs-evolution-css-in-js-bceae82b1fa0)

Because we’re living in the “component age,” witnessing the so-called CSS-in-JS (r)evolution, there are a lot of strong feelings and opinions surrounding CSS.

Component-based systems (i.e., Angular, React) introduced some new best practices, like building small, focused and independent components, splitting container and presentational components, having single-use CSS class names and so on.

React has a built-in pattern for sharing space between js, markup, and styles (via inline style). However, writing inline styles is lousy practice, feels unnatural, it lacks preprocessing support and makes it difficult to transfer from the stylesheets, nor to mention media queries or pseudo-elements. All of this created enough friction to keep styles separate. From this tension, the [CSS Modules](https://github.com/css-modules/css-modules) pattern developed.

Thanks to CSS Modules pattern popularity, we now have a large number of CSS-in-JS libraries with the mission to accomplish predictability and consistency of colocated styles.

* [CSS-in-JS](http://michelebertoli.github.io/css-in-js/)