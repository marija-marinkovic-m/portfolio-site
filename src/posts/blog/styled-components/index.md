---
title: Styled Components
draft: false
date: "2017-12-25"
seoImage: seo-img.png
tags: css,styled-components,BEM
---

Styled components (aka. visual primitives for the component age) is meant to be a successor to CSS Modules and a new way of writing dynamic CSS for the CSS folk, as it makes components the fundamental way to build a styled UI, making them styling constructs.
This builds a bridge between best practices that have been applied for years — like BEM and components as what a designer might use them for.

Apart from enforcing the best CSS-in-JS practices, styled components offer a bunch of great features, some of them:

*   tagged template literals
*   performace gains (high-speed rendering and first meaningful paint resolution) 
*   media queries, pseudo selectors, nesting... it all works
*   built-in theming
*   full React Native support


## How does it work?
__for people in a hurry:__

![Styled Components](./styled.png "Graphic from Steve Pietrek's great article http://spietrek.github.io/Styled-Components/")

Resources:
* http://spietrek.github.io/Styled-Components/
* https://blog.primehammer.com/the-performance-of-styled-react-components/
* https://www.smashingmagazine.com/2017/01/styled-components-enforcing-best-practices-component-based-systems/
* https://medium.com/@_alanbsmith/why-we-use-styled-components-at-decisiv-a8ac6e1507ac
* https://medium.com/styled-components/with-styled-components-into-the-future-d1d917e7c22c
* https://medium.com/drivetribe-engineering/why-were-slowly-migrating-to-styled-components-2f284d4b5d95
* https://www.youtube.com/watch?v=X_uTCnaRe94
* https://www.youtube.com/watch?v=XR6eM_5pAb0
* https://mxstbr.blog/2016/11/styled-components-magic-explained/
* https://medium.com/seek-blog/a-unified-styling-language-d0c208de2660



 