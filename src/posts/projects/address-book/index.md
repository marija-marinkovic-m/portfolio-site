---
title: Address book
draft: false
date: "2017-09-01"
seoImage: seo-img.png
productType: Admin Dashboard, Single Page Application
engagementPeriod: Jun 2017-Oct 2017
contribution: Data Organization, Material Design principles implementation, Front-end development
technologiesUsed: ReactJS, Material Design, Redux,Laravel
source: https://bitbucket.org/marija-marinkovic-m/addressbook
demo: http://previous-juice.surge.sh
demoCredentials: "Email:john@doe.com,Password:1234"
---

 | |
------------ | ------------- |
![widgets light theme](./widgets-light.png) | ![widgets dark theme](./widgets-black.png)

Serbian mountains are still unexplored, lonely and full of diversity. If we add sunshine peaks and fellow climbers to the equation, mountaineering turns out to be the perfect antidote to a sedentary lifestyle.

Mountaineering Association of Serbia has an extensive database of members, both legal and natural persons. We needed a customized solution for managing members and their membership fees, along with some necessary personal information. This new solution should replace the old managing system that became dysfunctional.

We created custom REST API (PHP, Laravel). Since time wasn't the most abundant resource, nor UI was team's primal preoccupation, I've decided to leverage some of the _good old_ frameworks for the front-end side of the project. [ReactJS](https://reactjs.org) in combination with [Material design](https://material.io/design) principles proved to be a right decision in the past. Also, because [Redux-Saga](https://redux-saga.js.org) is my favorite formula for application side effect management, I __npm-installed__ admin-on-rest (today's [react-admin](https://github.com/marmelab/react-admin)) package and started hewing and modeling it to fit our back-end API.

Besides the familiarity of redux-saga approach, MUI, and clean, customizable react components, admin-on-rest features [Redux Form](https://redux-form.com/7.3.0) and Airbnb's Polyglot.js. When all's said and done, I think this was the fastest and easiest route to take. There were quite a few bumps on that road, but the overall process of establishing communication between our back-end API with the adjusted admin-on-rest templates was convenient. 

### Landing desert

Users and Clubs are the resources of the first order (members: natural and legal persons), Membership resource is gluing them together. There are a dozen more resources, mainly descriptive but some of them carry viable functions.
After all these API resources were connected and tested with the front-end, I faced a daggering emptiness on the dashboard landing screen; there were no widgets, no content for widgets, except the extended _Search_ box and a few resource counters.

![Minimalizam](./emptyness.png)

Since I was working on the official Association's web [presentation](https://pss.rs), I thought it would be a good idea to turn there for some content inspiration. Soon I found event listings and news feeds to be suitable candidates for widgets.

Now, that the dashboard is thoroughly functional and looking acceptable, I can pack my comfy boots for maybe yet another mountain climbing adventure.