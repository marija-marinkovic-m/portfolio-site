---
title: Angular Wordpress Theme
draft: false
date: "2016-12-20"
seoImage: seo-img.png
productType: Admin Dashboard, Wordpress Theme, Single Page Application
engagementPeriod: Feb 2016-Apr 2016
contribution: Front-end development, Wordpress Theme development, XLS parsing
technologiesUsed: Angular.io, TypeScript, RxJS, PhpSpreadsheet, Wordpress CMS, WP API
source: https://marija-marinkovic-m@bitbucket.org/marija-marinkovic-m/wp-angular-theme-test-project
demo: http://dev-remax-intranet.pantheonsite.io
demoCredentials: "Username:rmx_admin,Password:XV76v@PdrbHy<VK$"
docs: http://dev-remax-intranet.pantheonsite.io/doc
---

 | | |
 ------ | ------ | ------ |
 ![events](./events-rmx.png) | ![login](./login.png) | ![rankings](./rankings.png)

When it first appeared, Angular changed my world. 
Few lines here and there and boom! Power of magic!

I haven't seen back then, but things were far from perfect, and constant JS community flux has lead to the announcement of Angular 2. Old angular we all loved and cherished would soon be deprecated -- but never forgotten 😢.

### The bee's knees

I attended 2016. **ng-conf**, witnessing that outraged, emotional fever among angular developers: "When what, why, who, because?". Panic levels and similar circumstances surrounding Angular framework at the time were wonderfully presented by Shai Reznik's [performance](https://www.youtube.com/watch?v=aSFfLVxT5vA).

Version 2 came out advertised as a platform for the future. It persisted in the beta state for almost a year, but I just couldn't wait to dig in. 

We were working on a custom WP theme at a time. The client needed a dashboard solution with different access levels for his company's employees. The most challenging part of the project was the _Agent Rankings_ leaderboard, weekly updated. Super admins should be doing updates by uploading spreadsheet templates to be parsed by the backend services, following strictly defined rules [rankings backend service](http://dev-remax-intranet.pantheonsite.io/wp-admin/users.php?page=agent_rankings_data). 

Agents once imported could be filtered and sorted by the award definitions, i.e.:
```
Diamond ------ 1
Platinum ----- 2
Gold --------- 3
Silver ------- 4
Bronze ------- 5
```

Here I saw a long-awaited opportunity to hitch the newborn NG horse to ride. I organized the code into three main categories, each living in the corresponding theme folder: 
* _Core Theme Files_ (user management, spreadsheet parser, general functions, etc.)
* _Endpoints_ (port to WP API customizations, CRUD operations)
* _Frontend scripts and markup_ (powered by Angular).

Most of my time I spent in the _Frontend scripts and markup_ folder, doing angular stuff.

The project was unfolding blazingly fast, without much friction. Although, I met some of the functional programming concepts (with [RxJS](http://reactivex.io/rxjs/) _observable_ flavor) for the first time. It was relatively easy to wrap my head around it, mainly because angular code samples and documentation were exceptional for newcomers. There I learned how versatile and useful these concepts were. 
Many of "formulas" I've picked back then are among my today's best practices.

Moreover, I can confirm the "bee's knees" aka [Zones](https://github.com/angular/zone.js/) are doing wonders for UX