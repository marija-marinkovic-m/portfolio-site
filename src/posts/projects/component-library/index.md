---
title: Component Library
draft: false
date: "2018-05-15"
seoImage: seo-img.png
productType: Reusable React Components Library, Living Documentation
engagementPeriod: May 2018-Work In Progress
contribution: Define and craft versatile building blocks for several ongoing projects, Tests, Interactive Documentation
technologiesUsed: ReactJS, Storybook
source: https://bitbucket.org/marija-marinkovic-m/component-library
demo: http://overt-jeans.surge.sh/?selectedKind=Getting%20Started&selectedStory=Overview&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybooks%2Fstorybook-addon-knobs
---

 | |
 ------ | ------ |
 ![buttons](./btn.png) | ![fontawesome icons](./icons.png)

There are many benefits of investing additional time to create a toolbox of highly customizable, reusable components, aka building blocks for various future projects.

### Consistency

Reusable components enforce UI consistency. It is critical to limit future developer’s and designer’s freedom to copy-paste code, or even start from scratch for every single application because this may result in a patchwork of different feels, looks, techs, moods and so on.

### Maintenance

Improper code scaling could drain more and more resources (not just time) to keep it in order and prevent malfunctioning, in other words, its maintenance may become a severe problem. More code equals more maintenance. Reusable components minimize the amount of code we need to produce and maintain.

### Performance

How to minimize application’s bundle size and memory usage? The obvious answer is, of course, the component library! Every time we include the component from the library, after the initial load, it requires no additional download and hardly any memory. Without a component library, some (un)conscious javascript duplications may show their ugly face and various solutions to the same problem could start appearing from the dark, thus cluttering not only production bundle but also a human mind. Trust me, I paid a high price to experience this kind of trouble.

### Need for speed

With the component library in order, further development, as well as design decision making should be a breeze. Do we need one more react application? No problem, we can focus on functionality and ship code in record time. Just include components from the library and arrange their layout. Done. Components eliminate decision fatigue by programmatically enforcing a standardized approach.

### Wind of change

Today, we’re using react, and our primary buttons have `1em` paddings and input field validation errors are `#be0000`, located under the field. Tomorrow, we might change over to [Vue](https://vuejs.org/), or vanilla ECMAScript7, our buttons might have to lose border-radius, and input field validation errors should be required to move to the left side of the field… I don’t know. 
But, I do know it will be much easier to migrate componentized app than a patchwork of different approaches and patterns and techs and daredevils. You just need to update/replace one existing component at a time.

To sum this up, a library of reusable components is the worthy investment. If anyone here believes otherwise, let them speak now or forever hold their peace. :bowtie: