---
title: Data Visualisation
draft: false
date: "2017-05-12"
seoImage: seo-img.png
productType: Single Page Application, Admin Dashboard
engagementPeriod: Mar 2017-Jun 2017
contribution: Front-end starter, Presentational part of the codebase
technologiesUsed: D3.js, React, Redux, MaterialUI
source: https://bitbucket.org/marija-marinkovic-m/react-material-dashboard
demo: http://fantastic-eggs.surge.sh
---

 | |
 ------ | ------ |
 ![donut](./donut.png) | ![funnel](./funel.png)

D3.js (Data-Driven Documents) is a JavaScript library for producing dynamic, interactive data visualizations. It is used on hundreds of thousands of websites and print publications.
D3 is capable of loading and parsing documents (i.e., importing data from CSV), it can transform data and bind data elements to the DOM, plus transitions. 

This robust player has many strengths, but so does React. What happens when these two titans are put on the case and forced to cooperate? 

![vertical](./vertical.png)

In case of this particular project, I've decided that D3 has to give up all of the DOM control and focus on Math. 

Here is the higher order component responsible for data visualisations (shortend for brevity) that requires `calculate` method implementation. 
```javascript
export const D3ComponentHoc = function(WrappedComponent) {
  return class extends WrappedComponent {
    componentWillMount() {
      if (!super.calculate) {
        throw new Error('`calculate` method required');
      }
      super.calculate(this.props);
    }
    componentWillReceiveProps(nextProps) {
      if (!super.calculate) {
        throw new Error('`calculate` method required');
      }
      super.calculate(nextProps)
    }
   [...]
  }
}
```
The `calculate` is the only method where D3 appears, performs calculations for SVG paths, scales, layouts and any transformations that take user data (for example [funnel calculator](https://bitbucket.org/marija-marinkovic-m/react-material-dashboard/src/master/src/shared/util/d3/funnelCalculator.js)). Results are handled back to React and its lifecycle methods are in control of the rest of the process.