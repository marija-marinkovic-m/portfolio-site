---
title: Detective QR Scans
draft: false
date: "2015-07-11"
seoImage: seo-img.png
productType: Smartphone/Tablet Application, Administration Dashboard
engagementPeriod: May 2016-June 2016
contribution: Design and Development
technologiesUsed: Firebase, TypeScript, Angular, PrimeUI, NativeScript
source: https://bitbucket.org/marija-marinkovic-m/detective-investigation
demo: https://csi-profile-app.firebaseapp.com
demoCredentials: "Username:marija.marinkovic.m@gmail.com,Password:jH6$JYrm"
---

> Looking for some fun forensic activities to do at home? From blood spatter analysis to DNA extractions, we've got fun (yet safe) activities for all ages.

![landing](./login-screen.png)

This project consists of two interconnected systems, one for creating and browsing crime scenery and dossiers, and the other for scanning barcodes produced by the former to locate the suspects.
The admin part of the project should be equipped with QR code generator and printer. QR code prints are to be used in the game scenery, probably masked or in some way hidden, as evidence material.

### Web application
I used [PrimeUI](https://www.primefaces.org) to bootstrap web app's styling and layout markup. Administration part of the application is connected to Firebase for managing data and printing QR codes. The one public route is used for reading specific dossier.

![planning](csi-component-tree.png)

> QR Codes are two-dimensional barcodes. QR stands for Quick Response which refers to the instant information access hidden in the Code. They are customizable, both in function and design, and are the best channel for connecting traditional print media with any interactive online content.
 
### QR Code scanner [source](https://bitbucket.org/marija-marinkovic-m/detective-investigation-native-app)

For this particular project, I've used [NativeScript](https://www.nativescript.org) with web view as a port to the main web application screens. [🔎 NativeScript QR](https://github.com/EddyVerbruggen/nativescript-barcodescanner) is an open source unicorn helper. With it and only a few lines of code, I've succeeded to reach out to device's camera and perform scans.

```javascript
import { Component } from '@angular/core';
import * as barcodescanner from 'nativescript-barcodescanner';
import * as utility from 'utils/utils';

@Component({
    selector: 'main-screen', 
    template: `
    <ActionBar title="CSI:Training"></ActionBar>
    <StackLayout verticalAlignment="center">
        <Image src="~/assets/img/logo-new.png" width="164" height="115"></Image>
        <Button text="Scan evidence" (tap)="startScan($event)"></Button>
        <Label [text]="scanResult" [ngStyle]="{'background-color': 'gray'}"></Label>
    </StackLayout>
    `
})
export class HomeScreenComponent {
    public scanResult:string = '';

    requestPermission() {
        return new Promise((resolve, reject) => {
            barcodescanner.available().then((available) => {
                if (available) {
                    barcodescanner.hasCameraPermission().then((granted) => {
                        if (!granted) {
                            barcodescanner.requestCameraPermission().then(() => {
                                resolve("Camera permission granted");
                            });
                        } else {
                            resolve("Camera permission was already granted.");
                        }
                    }); 
                } else {
                    reject("This device does not have an available camera");
                }
            });   
        });
    }
    
    startScan(event:any) {
        this.requestPermission().then((result) => {
            barcodescanner.scan({
                cancelLabel: "Stop scanning",
                message: "Place the evidence inside the viewfinder rectangle to scan it", 
                preferFrontCamera: false, 
                showFlipCameraButton: true, 
                orientation: "portrait"
            })
            .then((result) => {
               this.scanResult = result.text;
               utility.openUrl(result.text);
            }, (error) => {
                console.log('No scan: ' + error);
            })
        }, (error) => {
            console.log('ERROR', error);
        });
    }
}
```
Now, we're equipped to investigate crime scenes for clues and analyze evidence to catch the killers.   
Are you ready to prove your detective skills?