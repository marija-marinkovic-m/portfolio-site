---
title: Parfume Shop Checkout
draft: false
date: "2017-01-19"
seoImage: seo-img.png
productType: Online Shop, Single Page Application
engagementPeriod: Dec 2016-Jan 2017
contribution: Integrate existing codebase with any payment gateway to accept payments
technologiesUsed: oAuth, MustacheJS, LocalForage, WooCommerce REST API
source: https://bitbucket.org/marija-marinkovic-m/parfume-store
demo: http://dazzling-birth.surge.sh/products.html
---

![Checkout](./payment.png)

One day I got a nice static website codebase from my colleagues from Novi Sad. They used their homemade styling framework ([inuitcss](https://github.com/inuitcss/inuitcss) alike) and [Nunjucks](https://mozilla.github.io/nunjucks/) templating engine.

But this good-looking application was yet not eligible for making money. 
That was where I jump in.

### Shopping bag

First things first, this application is serverless, the best way I can manage user session in the current situation is probably IndexDB or any other browser storage API. So, I employed [LocalForage](https://github.com/localForage/localForage) 💾 to listen for and store relevant shopping information, and also to remove them when a customer completes the order.

### Payment Gateways

The application will remain serverless, but at some point in time, we have to ask some server to take care of the transactions on our behalf. So, I  added several functions to establish [OAuth1.0a](https://tools.ietf.org/html/rfc5849), a one-legged connection with server to fulfill shopping order.

Instead of plain XMLHttpRequest, I used [Axios](https://www.npmjs.com/package/axios) (supports Promise API, cross-browser ajax, interception and auto-transformation or request and responses) to communicate.

At the time, I didn't know better, so I suggested Woocommerce REST API as a mediator to any of available payment gateways. Now, I see that this could be done much better, with a direct connection between the web application and payment processor (Stripe, PayPal, Authorize.net,...), but back than, WooCommerce was comfortable and familiar, and some people are [lazy](https://www.psychologytoday.com/us/blog/hide-and-seek/201410/the-psychology-laziness). So, this prototype leveraged *Direct Bank Transfer* and *PayPal Express Checkout* methods.

> “Experience is merely the name men gave to their mistakes.” 
>  ― Oscar Wilde, The Picture of Dorian Gray