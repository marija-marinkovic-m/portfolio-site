---
title: Personal Address Book
draft: false
date: "2018-06-05"
seoImage: seo-img.png
productType: Single Page Application, design-Lonneke@Wonderkind
engagementPeriod: June 2018
contribution: Front-end development
technologiesUsed: Vue, Vuex, Jest, LocalForage, @vue/cli, blob-util
source: https://bitbucket.org/marija-marinkovic-m/wonderkind-address-book
demo: https://dist-tnebpozhod.now.sh
---

 | |
------------ | ------------- |
![new/edit screen](./edit.png) | ![search/main screen](./search.png)

The project was bootstrapped with [`@vue/cli`](https://github.com/vuejs/vue-cli) and I used [Jest](https://facebook.github.io/jest/) for testing. Application state is managed with [`Vuex`](https://vuex.vuejs.org/), store persistence is achived by leveraging Web Storage API.

## Technical requirements
- Single page application (SPA)
- Use proper build tools for build automation and testing.
- Application state should be persistent even when refreshing.
- 3 working tests are available.

## Functional requirements
- New contacts can be added
- Existing contacts can be edited or deleted
- A contact can have a name, phone number, email address and a physical address.
- By default, the address book should show the contacts visible in the design

### Avatars
Here, I played a little. So now, besides default fields (name, email, phone, mobile, and address), a user can upload and store an image file (avatar). Since the File interface is based on Blob, and IndexedDB supports Blobs, I arranged `AvatarInput.vue` component to handle upload and display of files from user's system.

```javascript
/// `@/components/shared/AvatarInput.vue`
import { createObjectURL } from 'blob-util';

export default {
  name: 'AvatarInput',
  props: {
    blob: File
  },
  computed: {
    avatarSrc () {
      return this.blob ? createObjectURL(this.blob) : defaultAvatarSrc;
    }
  },
  methods: {
    ...
  },
  components: {
    ...
  }
}
```


### Search
The search functionality is achieved by the `SortAlphabeticaly` mixin. This mixin does not just sort, but also grouping names by a surname first letter and makes group/sort results available to the presentational component via the `groupedCollection` computed parameter.

 ```javascript
 export const SortAlphabeticallyMixin = {
  beforeCreate () {
    if (!this.$options.computed) {
      this.$options.computed = {};
    }
    this.$options.computed.groupedCollection = {
      get() {
        const sortParam = this.$props.sortBy || 'id';
        const groupedObj = this.$props.items && this.$props.items
        .filter(item => item[sortParam])
        .reduce(
          (acc,n) => {
            const firstLetter = String(n[sortParam].charAt(0)).toLowerCase();
            acc[firstLetter] = (acc[firstLetter] || []).concat(n);
            return acc;
          },
          {}
        );
        return groupedObj && Object.keys(groupedObj)
          .sort()
          .reduce((a,n) => {
            a[n] = groupedObj[n];
            return a;
          }, {});
      }
    };
  }
};
 ```


### Snackbars
In order to enhance UX, I've also added kind of a `snackbar` interface, so you're informed about how app handled inputs, and if there are options available to undo critical actions.