---
title: Radio Streaming
draft: false
date: "2018-03-15"
seoImage: seo-img.png
productType: A Platform for Browsing and Listening regional radio stations
engagementPeriod: Jan 2018-Work In Progress
contribution: Front-end development
technologiesUsed: Web Audio API, React, Next.js, Express, Redux-Saga, JSS, Contentfull
source: https://bitbucket.org/marija-marinkovic-m/radiostations
demo: http://radiostanica.herokuapp.com
---

>The Media And The Mood Of The Nation study found that people who regularly watched television, used the computer, or listened to the radio were happier and had more energy than those that did not.
> -- [dailymail.co.uk](http://www.dailymail.co.uk/news/article-2009161/Why-listening-radio-gives-pleasure-watching-TV-using-laptop.html)

 | |
 ------ | ------ |
 ![contentful](./contentful.png) | ![radio station preview](./naxi.png)

__Radio Streaming__ is a sandbox project, in the _**proff-of-the-concept**_ phase. I wanted to experiment with using the [Web Audio API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API), around isomorphic JavaScript paradigm. Also, I [heard](https://medium.com/@MentallyFriendly/designing-and-building-quickly-for-the-web-5a118165a6f9) [Contentful](https://www.contentful.com/) is dominant, new-age CMS, so I wanted to take a bite.

Another inherent characteristic of this project is the extensive usage of Web Storage utilizing Mozilla's [localForage](https://github.com/localForage/localForage) mighty but straightforward API. History, Favorites, and even some of the specific stations' metadata are stored and pulled from browser's storage.