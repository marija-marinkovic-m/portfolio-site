---
title: Team Schedule
draft: false
date: "2016-03-02"
seoImage: seo-img.png
productType: Prototype, Sandboxing, Single Page Application, ToDo App, Productivity
engagementPeriod: Apr 2016-June 2016
contribution: Design, Front-end development
technologiesUsed: Angular.io, Firebase, PrimeUI, Electron, RxJS
source: https://bitbucket.org/marija-marinkovic-m/team-scheduler
demo: https://amber-fire-98.firebaseapp.com
demoCredentials: "Email:xmarijam@gmail.com,Password:BpI&NmaSpNUyx4tK2#"
---

| |
------ | ------ |
![app](./app-screen.png) | ![firebase](./firebase.png)

**Team Scheduler** is an amusing free time project I took to play with Electron and Firebase.

Team collaboration software is on the rise. More online collaboration tools spring up every year while existing ones are continually improving their features and functionality. Luckily, I never have had similar ambitions with this simple application.

