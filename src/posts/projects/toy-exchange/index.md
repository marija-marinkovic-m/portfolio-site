---
title: Toy Exchange
draft: true
date: "2017-06-10"
seoImage: seo-img.png
---
Toy-Exchange.org is an online community dedicated to the free exchange of used toys and games. Much like Freecycle, you can post offers of free used toys and games for other members. You can also post wanted items, and you can receive toys and games offered by others.

The community is organized into local user groups. Each group is geographically small enough that you won't have to travel far to pick up a toy or game. There is no shipping with Toy-Exchange.org. All posts in your home group will be within about a 5-mile radius, depending on your location. You may select to receive posts and notifications from nearby groups. This will increase the volume of used toys and games to choose from, but you may have to travel farther to get them.

When you register with Toy-Exchange.org, you will receive a free trial period, after which you will be asked to pay a $12 annual membership fee. There is no charge for giving or getting used toys and games.

Once registered, you can go to your Settings Page to create default settings for your experience on the site. You can select a default Pick Up Address, Pick Up Window and Pick Up Placement. This information will then autofill when you create posts. You can also choose to fill these out manually each time you create a new post. Your Pick Up Address will only be seen by the member you select to receive your item. Please read the Posting Guidelines for detailed information on how to create posts on our site.

You will also be able to select to receive individual notifications or a daily digest. Every time a member posts a toy or game in your home group – or in a nearby group if you have selected to receive notifications from nearby groups – an email notification with the Post Title, a photo (if submitted) of the item and a link back to the post will go out to group members.

If you want the item, you can follow the link and reply to the posting member that you are interested.

The offering member will select from among respondents. If you are selected to receive the item, you will receive an email with the Pick Up Address, the Pick Up Window – time that the item will be available, for example 10:00-12:00 Saturday–and Pick Up Placement, for example Front Porch. If the posting member has not indicated a specific Pick Up Window, but has instead stated something like, "Flexible," or "Email me," you will need to coordinate a pick up time with them.