---
title: WooCommerce Raiffeisen Extension
draft: true
date: "2014-10-03"
seoImage: seo-img.png
---

eCommerce Connect is the system of accepting Visa Ð¸ MasterCard payments online developed by Ukrainian Processing Center UPC in 2005. Developed by Ukrainian Processing Center UPC,  one of the largest processing centers in Central and Eastern Europe.

## Requirements
OpenSSL

## Installation
1. Download woocommerce-raiffeisen to your desktop.
1. If downloaded as a zip archive, extract the Plugin folder to your desktop.
1. Read through the "readme" file thoroughly to ensure you follow the installation instructions.
1. With your FTP program, upload the Plugin folder to the wp-content/plugins folder in your WordPress directory online.
1. Go to Plugins screen and find the newly uploaded Plugin in the list.
1. Click Activate to activate it.

## Integration
![raiffeisen-prinscreen.png](./seo-img.png)

### WooCommerce

### Release notes