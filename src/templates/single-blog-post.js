import React from 'react';
import get from 'lodash/get';
import { SeoMetaTags, Pagination, NextPost, Tags } from '../components';

export default class SinglePost extends React.Component {
  componentDidMount() {
    const aos = require('aos');
    const config = require('../assets/aos-config');
    aos.init(config);
  }

  render() {
    const { data } = this.props;
    const { frontmatter: { title, date, seoImage, tags }, html, fields: { path } } = data.markdownRemark;

    const meta = data.site.siteMetadata;
    const seoImageSrc = meta.siteUrl + get(seoImage, 'childImageSharp.resize.src');

    // pagination paths
    const edges = get(data, 'allMarkdownRemark.edges')
      .filter(edge => edge.node.fields.slug !== '404');
    const currentIndex = edges.findIndex(({node}) => get(node, 'fields.path') === path);
    const nextTitle = get(edges[currentIndex + 1], 'node.frontmatter.title');
    const nextPath = get(edges[currentIndex + 1], 'node.fields.path');
    const previousPath = get(edges[currentIndex - 1], 'node.fields.path');

    return (<div>
      <SeoMetaTags
        author={meta.author}
        datePublished={date}
        description={meta.description}
        facebookAppId={meta.facebookAppId}
        image={{url: seoImageSrc, width: 1000, height: 500}}
        logo={{url: `${meta.siteUrl}/logo-structured-data.png`, width: 600, height: 60}}
        publisher={meta.title}
        title={`${title} | ${meta.title}`}
        twitterHandle={meta.twitterHandle}
        type="article"
        url={meta.siteUrl + path} />
      <div id="main" className="single">
        <h1 data-aos="fade-up">{title}</h1>
        <p data-aos="fade-up" data-aos-delay="100">
          <em><i className="fa fa-calendar"></i>&nbsp;&nbsp;{date}</em>
        </p>
        <Pagination next={nextPath} index="/blog" previous={previousPath} />
        <div data-aos="fade-up" data-aos-delay="200" dangerouslySetInnerHTML={{__html: html}} />
        <Tags style={{marginBottom: '3.5rem'}} tags={tags.split(',')} />

        <NextPost data-aos="fade-up" data-aos-delay="300" to={nextPath} title={nextTitle} />
      </div>
    </div>);
  }
}

export const singleBlogPostQuery = graphql`
  query SingleBlogPostQuery($path: String!) {
    site {
      siteMetadata {
        title
        author
        description
        facebookAppId
        twitterHandle
        siteUrl
      }
    }
    markdownRemark(fields: {path: {eq: $path}}) {
      id
      html
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        tags
        seoImage {
          childImageSharp {
            resize(width: 1000, quality: 100) {
              src
            }
          }
        }
      }
      fields {
        path
      }
    }
    allMarkdownRemark (
      filter: {
        fields: {
          slug: {regex: "/blog\//"}
        },
        frontmatter: {
          draft: {ne: true}
        }
      }
      sort:{fields:[frontmatter___date], order:DESC}
    ) {
      edges {
        node {
          fields {
            path
            slug
          }
          frontmatter {
            title
          }
        }
      }
    }
  }
`;