import React from 'react';
import get from 'lodash/get';
import { CopyToClipboard } from 'react-copy-to-clipboard';

import { SeoMetaTags, Pagination, NextPost, Modal } from '../components';

const confirmationStyle = {
  position: 'absolute', zIndex: 3,
  top: '-70px', left: 0, right: 0
};


const Meta = ({productType, engagementPeriod, contribution, technologiesUsed}) => {
  if (!productType && !engagementPeriod && !contribution && !technologiesUsed) return null;

  return (
    <div className="row project-meta" data-aos="fade-up">
      <div className="work-item 3u 12u$(xsmall)">
        <h3>Goal</h3>
        <p>{ productType || '---' }</p>
      </div>
      <div className="work-item 3u 12u$(xsmall)">
        <h3>Period</h3>
        <p>{ engagementPeriod || '---' }</p>
      </div>
      <div className="work-item 3u 12u$(xsmall)">
        <h3>My Contribution</h3>
        <p>{ contribution || '---' }</p>
      </div>
      <div className="work-item 3u 12u$(xsmall)">
        <h3>Technologies Used</h3>
        <p>{ technologiesUsed || '---' }</p>
      </div>
    </div>
  );
}

export default class SinglePost extends React.Component {
  state = {
    isOpenCredentials: false,
    copied: ''
  }
  timeoutHandle = null

  componentDidMount() {
    const aos = require('aos');
    const config = require('../assets/aos-config');
    aos.init(config);
  }

  toggleCredentialsModal = () => {
    this.setState((prevState) => ({
      isOpenCredentials: !prevState.isOpenCredentials
    }))
  }

  copied = (fieldName) => {
    this.setState({
      copied: `Copied \`${fieldName}\``
    }, () => {
      if (this.timeoutHandle) window.clearTimeout(this.timeoutHandle);
      
      this.timeoutHandle = setTimeout(() => this.setState({copied: ''}), 3000);
    });
  }

  render() {
    const { data } = this.props;
    const {
      frontmatter: {
        title, date, seoImage,
        source, demo, demoCredentials, docs,
        ...otherFrontmatter
      },
      html, fields: { path } } = data.markdownRemark;

    const meta = data.site.siteMetadata;
    const seoImageSrc = meta.siteUrl + get(seoImage, 'childImageSharp.resize.src');

    // pagination paths
    const edges = get(data, 'allMarkdownRemark.edges')
      .filter(edge => edge.node.fields.slug !== '404');
    const currentIndex = edges.findIndex(({node}) => get(node, 'fields.path') === path);
    const nextTitle = get(edges[currentIndex + 1], 'node.frontmatter.title');
    const nextPath = get(edges[currentIndex + 1], 'node.fields.path');
    const previousPath = get(edges[currentIndex - 1], 'node.fields.path');

    return (<div>
      <SeoMetaTags
        author={meta.author}
        datePublished={date}
        description={meta.description}
        facebookAppId={meta.facebookAppId}
        image={{url: seoImageSrc, width: 1000, height: 500}}
        logo={{url: `${meta.siteUrl}/logo-structured-data.png`, width: 600, height: 60}}
        publisher={meta.title}
        title={`${title} | ${meta.title}`}
        twitterHandle={meta.twitterHandle}
        type="article"
        url={meta.siteUrl + path} />
      <div id="main" className="single project">
        <h1 data-aos="fade-up">
          <span>{title}&nbsp;</span>

          {/* source url (usually bitbucket) */}
          { source && <a href={source} title="Source code" target="_blank"><i className="fa fa-code"></i>&nbsp;Source&nbsp;</a> }

          {/* demo url */}
          { demo && <a href={demo} title="Preview" target="_blank"><i className="fa fa-laptop"></i> Demo&nbsp;</a> }

          {/* credentials popup trigger */}
          { demoCredentials && <a
            href="#credentials-modal"
            title="Credentials Popup"
            onClick={this.toggleCredentialsModal}>
            <i className="fa fa-key"></i>&nbsp;Demo credentials&nbsp;
          </a> }

          {/* documentation url */}
          { docs && <a href={docs} title="Documentation" target="_blank"><i className="fa fa-book"></i>&nbsp;Docs&nbsp;</a> }

        </h1>
        <Pagination next={nextPath} index="/projects" previous={previousPath} />


        { <Meta {...otherFrontmatter} /> }


        <div data-aos="fade-up" data-aos-delay="200" dangerouslySetInnerHTML={{__html: html}} />
        <NextPost data-aos="fade-up" data-aos-delay="300" to={nextPath} title={nextTitle} />


        { this.state.isOpenCredentials && <Modal
          close={this.toggleCredentialsModal}
          title="Demo Credentials">

          { this.state.copied && <p
            className="align-center alert"
            style={confirmationStyle}
            data-aos="fade-up"><i>{this.state.copied}</i></p> }

          {
            demoCredentials.split(',')
              .map((c,i) => {
                const [name, value] = c.split(':');

                return (
                  <div
                    key={i}
                    className="inline-copy">
                    <label>{name}</label>
                    <input type="text" readOnly defaultValue={value} />
                    <CopyToClipboard
                      text={value}
                      onCopy={this.copied.bind(null, name)}>
                      <button
                        className="button icon-button">
                        <i className="fa fa-clone"></i>
                        <span className="sr-only">Copy {name} field</span>
                      </button>
                    </CopyToClipboard>
                  </div>
                );
              })
          }

          <button
            onClick={this.toggleCredentialsModal}
            type="button"
            className="button">
            Close
          </button>
        </Modal> }
      </div>
    </div>);
  }
}

export const singleProjectQuery = graphql`
  query SingleProjectQuery($path: String!) {
    site {
      siteMetadata {
        title
        author
        description
        facebookAppId
        twitterHandle
        siteUrl
      }
    }
    markdownRemark(fields: {path: {eq: $path}}) {
      id
      html
      frontmatter {
        title
        date(formatString: "YYYY, MMMM")
        seoImage {
          childImageSharp {
            resize(width: 1000, quality: 100) {
              src
            }
          }
        }
        productType
        engagementPeriod
        contribution
        technologiesUsed
        source
        demo
        demoCredentials
        docs
      }
      fields {
        path
      }
    }
    allMarkdownRemark (
      filter: {
        fields: {
          slug: {regex: "/projects\//"}
        },
        frontmatter: {
          draft: {ne: true}
        }
      }
      sort:{fields:[frontmatter___date], order:DESC}
    ) {
      edges {
        node {
          fields {
            path
            slug
          }
          frontmatter {
            title
          }
        }
      }
    }
  }
`;